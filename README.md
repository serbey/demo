# Summary

The purpose of this repository is to answer the backend interview at OneUp

## Exercise 1: TV Series REST API

I created a public RESTful API server to wrap two of TVDB API in two endpoints.

**The first endpoint is:** `GET /series/:id` to fetch a series information by its id.

This API doesn't requires Authorization.

*It returns 404 error in case there is no series found for the id requested.* 

*A 500 error should be returned in case something went wrong with TVDB API.* 
  
**The second endpoint is:** `GET /series/search` to get all available series searched by name:
```
{
	"name": "brooklyn",
	"page": 0, //optional
    "size": 4  //optional
}
```
This API requires Authorization header with value = "Bearer " + access_token

*As you can see, a pagination system is implemented for this API*

*we start with page = 0 and the size parameter is the number of search result inside one page.*

*If page and size are not provided. The full list of search is returned.*

*In case one of page or size values is not provided, the endpoint would return a 400 error.* 

*In case the name parameter is not provided, the endpoint would return a 400 error.* 

*In case of bad authentication, we get 401 error*

*In case there is no series for the name requested, we get a 404 error.* 

*A 500 error should be returned in case something went wrong with TVDB API.*

# Application Start / Application Test

It is not hard to run the application **DemoApplication.main** on IntelliJ

Unit tests on the two endpoints can be checked too by running **SeriesControllerTest.java**. 

There are 7 test that check most cases of the error handler and cover both endpoints.

Keep in mind that we need an access token to work with the search endpoint. An access token can be retrived from
`POST https://api.thetvdb.com/login` using the following body:
```
{ 
    "apikey": "4f61cb9c2f9e437b40d6c7d7832dcd0c", 
    "userkey": "5EAB0AE802E413.00848705", 
    "username": "oneup-interview" 
}
```
The access token retrieved should be used in the header of the search endpoint (Bearer + access_token) and should be included in *application.properties* in access_token variable. In fact the access token from *application.properties* is used for unit testing.

## Exercise 2: Legacy Refactoring

### 1) Code review

The co-worker mistakes are:

- He used the found boolean to check for matching products. This can be optimized as we can add the order to the list once the product is found in it. When a product match happen, we just break out of the loop and check result of the next order.
- In the first loop, he used list.get(0) while he should use list.get(i) as he is iterating through the list.
- He could have just created a linkedlist of order and add to it all the orders having the p product. What he did is he converted arraylist to linkedlist after working with arraylist.
- He checked for order != null after reading the order instance to check its products. This null check on the order is useless as the variable is not null for sure.

### 2) Code refactoring

My proposed solution would be:

```
public LinkedList findOrdersForProduct(Product p, boolean debug) {
	//create LinkedList from the start and return it at the end after filling it
    LinkedList<Order> l=new LinkedList<Order>();
	ArrayList list = getAllOrders();
	for (int i = 0; i < list.size(); i++) {
        // should access ith instance of list and not looping the 0 instance
		Order order = (Order) list.get(i); 
		// we can check for order not null before accessing its getProducts() function
        if (order != null && order.getProducts().size() > 0) { 
            for (int j = 0; j <= order.getProducts().size(); j++) {
				Product p2 = order.getProducts().get(j);
				if (p2 == p) {
					l.add(order);
                    // it's better to use break instead of found boolean
					break;
				}
			}
		}
	}
	return l;
}
```
