package com.example.demo.controllers;

import com.example.demo.exceptions.CustomException;
import com.example.demo.handlers.TVDBErrorHandler;
import com.example.demo.models.SearchResponse;
import com.example.demo.models.Series;
import com.example.demo.models.SeriesDetails;
import com.example.demo.models.SeriesResponse;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Optional;


@RestController
@RequestMapping("/series")
public class SeriesController {

    private RestTemplate restTemplate = new RestTemplateBuilder()
            .errorHandler(new TVDBErrorHandler())
            .build();

    @GetMapping("/search")
    public Series[] searchSeries(@RequestHeader String Authorization, @RequestParam String name, @RequestParam Optional<Integer> page, @RequestParam Optional<Integer> size) {
        //Both parameters, page & size, should be provided for pagination
        if(page.isPresent() && !size.isPresent()) {
            throw new CustomException("Missing size parameters for pagination");
        } else if(!page.isPresent() && size.isPresent()) {
            throw new CustomException("Missing page parameters for pagination");
        }
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", Authorization);
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        String url = "https://api.thetvdb.com/search/series?name="+name;
        ResponseEntity<SearchResponse> response = restTemplate.exchange(url, HttpMethod.GET, entity, SearchResponse.class);
        Series[] fullContent = response.getBody().getData();
        //return a paginated result if page & size is provided
        if(page.isPresent() && size.isPresent()) {
            int startIndex = page.get() * size.get();
            int endIndex = startIndex + size.get();
            if(startIndex >= fullContent.length) {
                throw new CustomException("Out of index pagination. Only " + fullContent.length + " series present for name=" + name);
            } else if(endIndex > fullContent.length) {
                endIndex = fullContent.length;
            }
            System.out.println(startIndex);
            Series[] pageContent = Arrays.copyOfRange(
                    response.getBody().getData(), startIndex, endIndex
            );
            return pageContent;
        }
        //else return full result without pagination
        return fullContent;
    }

    @GetMapping("/{id}")
    public SeriesDetails getSeries(@PathVariable String id) {
        String url = "https://api.thetvdb.com/series/"+id;
        SeriesResponse response = restTemplate.getForObject(url, SeriesResponse.class);
        return response.getData();
    }
}