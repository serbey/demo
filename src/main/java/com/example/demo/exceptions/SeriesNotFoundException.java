package com.example.demo.exceptions;

public class SeriesNotFoundException extends RuntimeException {

    public SeriesNotFoundException(String message) {
        super(message);
    }

}