package com.example.demo.handlers;

import com.example.demo.exceptions.*;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

public class TVDBErrorHandler implements ResponseErrorHandler {
    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        switch (response.getRawStatusCode()) {
            case 401:
                throw new UnauthorizedException("TVDB REST API credentials are not valid");
            case 404:
                throw new SeriesNotFoundException("The series is not found");
            default:
                throw new ServerErrorException("Something went wrong");
        }
    }

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        return response.getRawStatusCode() != 200;
    }
}
