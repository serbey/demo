package com.example.demo.models;

import java.util.Arrays;
public class SearchResponse {

    private Series[] data;
    public Series[] getData() {
        return data;
    }

    public void setData(Series[] data) {
        this.data = data;
    }

}
