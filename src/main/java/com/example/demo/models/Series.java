package com.example.demo.models;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Series {
    public int id;
    public String seriesName;
    public String overview;
    public String banner;
}
