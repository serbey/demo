package com.example.demo.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class SeriesDetails {
    public int id;
    public String seriesName;
    public String overview;
    public String banner;
    public String season;
    public String poster;
    public String fanart;
    public String status;
    public String firstAired;
    public String network;
    public String[] genre;
    public float siteRating;
    public int siteRatingCount;
}
