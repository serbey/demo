package com.example.demo.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(SeriesController.class)
public class SeriesControllerTest {

    @Value("${access_token}")
    private String accessToken;
    @Autowired
    private MockMvc mvc;

    @Test
    public void canSearchSeriesByName() throws Exception {
        //search series with name=brooklyn
        MockHttpServletResponse response = mvc.perform(
                get("/series/search?name=brooklyn")
                        .header("Authorization", "Bearer "+ accessToken)
                        .accept(MediaType.APPLICATION_JSON))
                        .andReturn().getResponse();
        //get response = 200
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    public void returnSearchNotFound() throws Exception {
        //search series with name=qwe123
        MockHttpServletResponse response = mvc.perform(
                get("/series/search?name=qwe123")
                        .header("Authorization", "Bearer "+ accessToken)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();
        //check if response = 404
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void returnUnauthorizedForSearch() throws Exception {
        //search series with name=brooklyn but bad authorization
        MockHttpServletResponse response = mvc.perform(
                get("/series/search?name=brooklyn")
                        .header("Authorization", "Bearerx "+ accessToken)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();
        //check if response = 401 unauthorized
        assertThat(response.getStatus()).isEqualTo(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    public void paginationRequiresPageAndSize() throws Exception {
        //try pagination with only page parameter
        MockHttpServletResponse response = mvc.perform(
                get("/series/search?name=brooklyn&page=20")
                        .header("Authorization", "Bearer "+ accessToken)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();
        //check if response = 400
        assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void paginationCantExceedResultSize() throws Exception {
        //try pagination with page = 5 size = 20 for name = brooklyn
        MockHttpServletResponse response = mvc.perform(
                get("/series/search?name=brooklyn&page=5&size=20")
                        .header("Authorization", "Bearer "+ accessToken)
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();
        //check if response = 400
        assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void canRetrieveSeriesById() throws Exception {
        // call get series api where id = 279884
        MockHttpServletResponse response = mvc.perform(
                get("/series/279884")
                        .accept(MediaType.APPLICATION_JSON))
                        .andReturn().getResponse();
        // check if response = 200
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    public void returnSeriesNotFound() throws Exception {
        // call get series api where id = 2
        MockHttpServletResponse response = mvc.perform(
                get("/series/2")
                        .accept(MediaType.APPLICATION_JSON))
                        .andReturn().getResponse();
        // check if response = 404
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
    }


}